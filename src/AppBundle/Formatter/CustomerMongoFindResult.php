<?php

namespace AppBundle\Formatter;

use AppBundle\Interfaces\Formatter as FormatterInterface;

class CustomerMongoFindResult implements FormatterInterface
{
    /**
     * Retorna um array com os dados formatados (Customer mongo find)
     *
     * @param  array $result Resultado do find do redis
     * @return array
     */
    public function format($result)
    {
        $newResult = array();
        foreach ($result as $customer) {
            $customer = json_decode($customer);
            $customerId = $customer->_id->{'$id'};
            $mongoId = new \MongoId($customerId);
            $newResult[$customerId] = array();
            $newResult[$customerId]['_id'] = $mongoId;
            $newResult[$customerId]['name'] = $customer->name;
            $newResult[$customerId]['age'] = $customer->age;
        }
        return $newResult;
    }
}
