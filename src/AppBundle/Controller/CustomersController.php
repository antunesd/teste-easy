<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use AppBundle\Service\DataManipulator;
use AppBundle\Formatter\CustomerMongoFindResult;

class CustomersController extends Controller
{
    /**
     * @Route("/customers/")
     * @Method("GET")
     */
    public function getAction()
    {
        $cacheService = $this->get('cache_service');
        $cacheService->setFindResultFormatter(new CustomerMongoFindResult());

        $databaseService = $this->get('database_service');

        $dataManipulator = new DataManipulator();
        $dataManipulator->addManipulator($cacheService);
        $dataManipulator->addManipulator($databaseService);
        $customers = $dataManipulator->retrieveData('customers');

        return new JsonResponse($customers);
    }

    /**
     * @Route("/customers/")
     * @Method("POST")
     */
    public function postAction(Request $request)
    {
        $customers = json_decode($request->getContent());

        if (empty($customers)) {
            return new JsonResponse(['status' => 'No donuts for you'], 400);
        }

        try {
            $cacheService = $this->get('cache_service')
                ->connect()
                ->setCollectionName('customers');

            $databaseService = $this->get('database_service')
                ->connect()
                ->setCollectionName('customers');

            foreach ($customers as $customer) {
                $databaseService->insert($customer);
                $cacheService->insert(json_encode($customer));
            }
        } catch (\Exception $e) {
            return new JsonResponse(['status' => utf8_encode($e->getMessage())], 500);
        }

        return new JsonResponse(['status' => 'Customers successfully created'], 201);
    }

    /**
     * @Route("/customers/")
     * @Method("DELETE")
     */
    public function deleteAction()
    {
        try {
            $cacheService = $this->get('cache_service')
                ->connect()
                ->setCollectionName('customers');

            $databaseService = $this->get('database_service')
                ->connect()
                ->setCollectionName('customers');

            $cacheService->drop();
            $databaseService->drop();
        } catch (\Exception $e) {
            return new JsonResponse(['status' => utf8_encode($e->getMessage())], 500);
        }

        return new JsonResponse(['status' => 'Customers successfully deleted']);
    }
}
