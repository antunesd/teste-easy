<?php

namespace AppBundle\Service;

use AppBundle\Interfaces\DataManipulator as DataManipulatorInterface;
use AppBundle\Interfaces\Formatter as FormatterInterface;

abstract class AbstractDataManipulator implements DataManipulatorInterface
{
    protected $client;
    protected $connectionParams;
    protected $collectionName = "default";
    protected $findFormatter;

    /**
     * Nome da collection dos dados (Redis key, MongoDb Collection)
     *
     * @param string $collectionName
     */
    public function setCollectionName($collectionName)
    {
        $this->collectionName = $collectionName;
        return $this;
    }

    /**
     * Retorna o nome da collection
     *
     * @return string
     */
    public function getCollectionName()
    {
        return $this->collectionName;
    }

    /**
     * Seta o Client que realizará as buscas
     *
     * @param mixed $client
     * @return AbstractDataManipulator
     */
    public function setClient($client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * Retorna o Client
     *
     * @return mixed
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Seta um formatador para o metodo find
     *
     * @param FormatterInterface $findFormatter
     */
    public function setFindResultFormatter(FormatterInterface $findFormatter)
    {
        $this->findFormatter = $findFormatter;
    }

    /**
     * Retorna o resultado formatado a partir de um FormatterInterface
     *
     * @param  mixed $result
     * @return mixed
     */
    public function getFindResultFormatted($result)
    {
        if ($this->findFormatter instanceof FormatterInterface) {
            return $this->findFormatter->format($result);
        }
        return $result;
    }
}
