<?php

namespace AppBundle\Service;

use AppBundle\Interfaces\DataManipulator as DataManipulatorInterface;

class DataManipulator
{
    private $dataManipulators = array();

    /**
     * Adiciona um DataManipulatorInterface no array
     *
     * @param DataManipulatorInterface $dataManipulator
     */
    public function addManipulator(DataManipulatorInterface $dataManipulator)
    {
        $this->dataManipulators[] = $dataManipulator;
    }

    /**
     * Retorna os manipuladores adicionados
     *
     * @return array
     */
    public function getManipulators()
    {
        return $this->dataManipulators;
    }

    /**
     * Faz o failover na busca de dados do cache ou banco adicionado
     * Se o primeiro da ordem falhar, chama o próximo
     *
     * @param  string $collectionName Nome da collection/key a ser buscada
     * @return mixed
     */
    public function retrieveData($collectionName)
    {
        foreach ($this->getManipulators() as $dataManipulator) {
            try {
                return $this->retrieveFromManipulator($dataManipulator, $collectionName);
            } catch (\Exception $e) {
                // Adicionar log para o failover, pode ocorrer erros de conexoes ou de query
                // echo (string)$e;
                continue;
            }
        }
    }

    /**
     * Realiza a busca conectando e setando a collection
     *
     * @param  DataManipulatorInterface $dataManipulator
     * @param  string $collectionName
     * @return mixed
     */
    private function retrieveFromManipulator(DataManipulatorInterface $dataManipulator, $collectionName)
    {
        return $dataManipulator
            ->connect()
            ->setCollectionName($collectionName)
            ->find();
    }
}
