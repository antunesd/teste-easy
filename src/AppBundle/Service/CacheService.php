<?php

namespace AppBundle\Service;

use \Predis\Client as RedisClient;

class CacheService extends AbstractDataManipulator
{
    /**
     * @param string  $host
     * @param string  $port
     * @param string  $prefix
     * @param integer $database
     */
    public function __construct($host, $port, $prefix, $database = 0)
    {
        $this->connectionParams = array (
            "host" => $host,
            "port" => $port,
            "prefix" =>$prefix,
            "database" => $database
        );
    }

    /**
     * Realiza a conexão com o cache
     *
     * @return CacheService
     */
    public function connect()
    {
        $this->setClient(
            new RedisClient($this->connectionParams)
        );
        return $this;
    }

    /**
     * Busca os dados do cache com base na key informada (collectionName)
     *
     * @return mixed
     */
    public function find()
    {
        return $this->getFindResultFormatted(
            $this->getClient()->lrange($this->getCollectionName(), 0, -1)
        );
    }

    /**
     * Insere dados no cache com base na key informada (collectionName)
     *
     * @param  mixed $data
     * @return bool
     */
    public function insert($data)
    {
        return $this->getClient()->lpush($this->getCollectionName(), $data);
    }

    /**
     * Remove os dados do cache com base na key informada (collectionName)
     *
     * @return bool
     */
    public function drop()
    {
        return $this->getClient()->del($this->getCollectionName());
    }
}
