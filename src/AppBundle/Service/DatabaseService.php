<?php

namespace AppBundle\Service;

use \MongoClient;

class DatabaseService extends AbstractDataManipulator
{
    protected $database;

    /**
     * @param string $host
     * @param string $port
     * @param string $database
     */
    public function __construct($host, $port, $database)
    {
        $this->connectionParams = array(
            "connString" => "mongodb://$host:$port/",
            "database" => $database
        );
    }

    /**
     * Conecta e seleciona a base do banco a ser usada
     *
     * @return DatabaseService
     */
    public function connect()
    {
        $this->setClient(
            new MongoClient($this->connectionParams["connString"])
        );

        $this->setDatabase(
            $this->getClient()->selectDB($this->connectionParams["database"])
        );

        return $this;
    }

    /**
     * Seta a base de dados
     *
     * @param \MongoDb $database
     * @return DatabaseService
     */
    public function setDatabase(\MongoDb $database)
    {
        $this->database = $database;
        return $this;
    }

    /**
     * Retorna a base selecionada
     *
     * @return \MongoDb
     */
    public function getDatabase()
    {
        return $this->database;
    }

    /**
     * Busca os dados do banco com base na collection
     *
     * @return array
     */
    public function find()
    {
        return iterator_to_array(
            $this->getDatabase()->{$this->getCollectionName()}->find()
        );
    }

    /**
     * Insere dados no banco com base na collection
     *
     * @param  mixed $data
     * @return bool|array
     */
    public function insert($data)
    {
        return $this->getDatabase()->{$this->getCollectionName()}->insert($data);
    }

    /**
     * Deleta os dados do banco com base na collection
     *
     * @param  mixed $data
     * @return bool
     */
    public function drop()
    {
        return $this->getDatabase()->{$this->getCollectionName()}->drop();
    }
}
