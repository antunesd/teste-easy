<?php

namespace AppBundle\Tests\Formatter;

use AppBundle\Formatter\CustomerMongoFindResult;

class CustomerMongoFindResultTest extends \PHPUnit_Framework_TestCase
{
    public function testClassShouldExists()
    {
        $this->assertInstanceOf(
            'AppBundle\Formatter\CustomerMongoFindResult',
            new CustomerMongoFindResult()
        );
    }

    public function testMethodFormatShouldReturnAnEmptyArray()
    {
        $result = array();
        $customerMongoFindResult = new CustomerMongoFindResult();
        
        $this->assertEquals(
            array(),
            $customerMongoFindResult->format($result)
        );
    }

    public function testMethodFormatShouldReturnANewArrayFormatted()
    {
        $idClass = new \stdClass();
        $idClass->{'$id'} = '51e1eefc065f908c10000411';
        $customer = new \stdClass();
        $customer->_id = $idClass;
        $customer->name = 'Teste';
        $customer->age = '10000';
        $result = array(json_encode($customer));

        $expected = array(
            '51e1eefc065f908c10000411' => array(
                '_id' => new \MongoId('51e1eefc065f908c10000411'),
                'name' => 'Teste',
                'age' => '10000',
            )
        );

        $customerMongoFindResult = new CustomerMongoFindResult();
        $this->assertEquals(
            $expected,
            $customerMongoFindResult->format($result)
        );
    }
}
