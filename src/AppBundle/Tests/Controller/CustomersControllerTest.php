<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CustomersControllerTest extends WebTestCase
{
    protected $client;

    public function setUp()
    {
        $this->client = static::createClient();
        $this->client->followRedirects();
    }

    public function testCreateCustomers()
    {
        $customers = [
            ['name' => 'Leandro', 'age' => 26]
        ];
        $customers = json_encode($customers);

        $this->client->request('POST', '/customers/', [], [], ['CONTENT_TYPE' => 'application/json'], $customers);

        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }

    public function testNoDonutsForMe()
    {
        $customers = [];
        $customers = json_encode($customers);

        $this->client->request('POST', '/customers/', [], [], ['CONTENT_TYPE' => 'application/json'], $customers);
        $response = $this->client->getResponse();

        $this->assertTrue($response->isClientError());
        $this->assertEquals('{"status":"No donuts for you"}', $response->getContent());
    }

    public function testGetCustomers()
    {
        $customers = [
            ['name' => 'Leandro', 'age' => 26]
        ];

        $this->client->request('GET', '/customers/', [], [], ['CONTENT_TYPE' => 'application/json']);
        
        $this->assertTrue($this->client->getResponse()->isSuccessful());

        $this->assertTrue(
            is_object(
                json_decode(
                    $this->client->getResponse()->getContent()
                )
            )
        );
    }

    public function testeDeleteCustomers()
    {
        $this->client->request("DELETE", '/customers/', [], []);
        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }
}
