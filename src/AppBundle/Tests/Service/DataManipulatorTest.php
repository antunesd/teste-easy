<?php

namespace AppBundle\Tests\Service;

use AppBundle\Service\DataManipulator;

class DataManipulatorTest extends \PHPUnit_Framework_TestCase
{
    public function testClassShouldExists()
    {
        $this->assertInstanceOf('AppBundle\Service\DataManipulator', new DataManipulator());
    }

    public function testMethodAddManipulatorShouldExecuteCorrectly()
    {
        $databaseServiceMock = $this->getMockBuilder('AppBundle\Service\DatabaseService')
            ->disableOriginalConstructor()
            ->getMock();

        $dataManipulator = new DataManipulator();
        $dataManipulator->addManipulator($databaseServiceMock);

        $this->assertEquals(
            array($databaseServiceMock),
            $dataManipulator->getManipulators()
        );
    }

    public function testMethodRetrieveDataShouldReturnAJsonFromCache()
    {
        $cacheServiceMock = $this->getMockBuilder('AppBundle\Service\CacheService')
            ->disableOriginalConstructor()
            ->getMock();
            
        $cacheServiceMock->expects($this->once())
            ->method('connect')
            ->willReturn($cacheServiceMock);

        $cacheServiceMock->expects($this->once())
            ->method('setCollectionName')
            ->willReturn($cacheServiceMock);

        $cacheServiceMock->expects($this->once())
            ->method('find')
            ->willReturn(array());

        $dataManipulator = new DataManipulator();
        $dataManipulator->addManipulator($cacheServiceMock);

        $this->assertTrue(
            is_array($dataManipulator->retrieveData('customers'))
        );
    }

    public function testMethodRetrieveDataShouldCatchAnExceptionFromRedisAndReturnAJsonFromBatabase()
    {
        $cacheServiceMock = $this->getMockBuilder('AppBundle\Service\CacheService')
            ->disableOriginalConstructor()
            ->getMock();

        $cacheServiceMock->expects($this->once())
            ->method('connect')
            ->willReturn($cacheServiceMock);

        $cacheServiceMock->expects($this->once())
            ->method('setCollectionName')
            ->willReturn($cacheServiceMock);

        $cacheServiceMock->expects($this->once())
            ->method('find')
            ->will($this->throwException(new \Exception()));

        $databaseServiceMock = $this->getMockBuilder('AppBundle\Service\DatabaseService')
            ->disableOriginalConstructor()
            ->getMock();

        $databaseServiceMock->expects($this->once())
            ->method('connect')
            ->willReturn($databaseServiceMock);

        $databaseServiceMock->expects($this->once())
            ->method('setCollectionName')
            ->willReturn($databaseServiceMock);

        $databaseServiceMock->expects($this->once())
            ->method('find')
            ->willReturn(array());

        $dataManipulator = new DataManipulator();
        $dataManipulator->addManipulator($cacheServiceMock);
        $dataManipulator->addManipulator($databaseServiceMock);

        $this->assertTrue(
            is_array($dataManipulator->retrieveData('customers'))
        );
    }


    public function testMethodRetrieveDataShouldCatchAnExceptionAndReturnNull()
    {
        $cacheServiceMock = $this->getMockBuilder('AppBundle\Service\CacheService')
            ->disableOriginalConstructor()
            ->getMock();

        $cacheServiceMock->expects($this->once())
            ->method('connect')
            ->willReturn($cacheServiceMock);

        $cacheServiceMock->expects($this->once())
            ->method('setCollectionName')
            ->willReturn($cacheServiceMock);

        $cacheServiceMock->expects($this->once())
            ->method('find')
            ->will($this->throwException(new \Exception()));

        $dataManipulator = new DataManipulator();
        $dataManipulator->addManipulator($cacheServiceMock);

        $this->assertTrue(
            $dataManipulator->retrieveData('customers') === null
        );
    }
}
