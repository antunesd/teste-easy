<?php

namespace AppBundle\Tests\Service;

use AppBundle\Service\AbstractDataManipulator;

class AbstractDataManipulatorTest extends \PHPUnit_Framework_TestCase
{
    public function testSetCollectionName()
    {
        $abstractDataManipulatorMock = $this->getMockForAbstractClass('AppBundle\Service\AbstractDataManipulator');

        $this->assertEquals(
            $abstractDataManipulatorMock,
            $abstractDataManipulatorMock->setCollectionName("teste")
        );

        $this->assertEquals(
            "teste",
            $abstractDataManipulatorMock->getCollectionName()
        );
    }

    public function testSetClient()
    {
        $mongoClientMock = $this->getMockBuilder('MongoClient')
            ->disableOriginalConstructor()
            ->getMock();

        $abstractDataManipulatorMock = $this->getMockForAbstractClass('AppBundle\Service\AbstractDataManipulator');

        $this->assertEquals(
            $abstractDataManipulatorMock,
            $abstractDataManipulatorMock->setClient($mongoClientMock)
        );

        $this->assertEquals(
            $mongoClientMock,
            $abstractDataManipulatorMock->getClient()
        );
    }

    public function testMethodGetFindResultFormattedShouldNotCallTheFormatter()
    {
        $customerMongoFindResultMock = $this->getMockBuilder('AppBundle\Formatter\CustomerMongoFindResult')->getMock();
        $customerMongoFindResultMock->expects($this->never())
            ->method('format')
            ->willReturn(array());

        $abstractDataManipulatorMock = $this->getMockForAbstractClass('AppBundle\Service\AbstractDataManipulator');

        $this->assertEquals(
            array(),
            $abstractDataManipulatorMock->getFindResultFormatted(array())
        );
    }

    public function testMethodGetFindResultFormattedShouldCallTheFormatter()
    {
        $abstractDataManipulatorMock = $this->getMockForAbstractClass('AppBundle\Service\AbstractDataManipulator');
        $customerMongoFindResultMock = $this->getMockBuilder('AppBundle\Formatter\CustomerMongoFindResult')->getMock();

        $customerMongoFindResultMock->expects($this->once())
            ->method('format')
            ->willReturn(array());

        $abstractDataManipulatorMock->setFindResultFormatter(
            $customerMongoFindResultMock
        );

        $this->assertEquals(
            array(),
            $abstractDataManipulatorMock->getFindResultFormatted(array())
        );
    }
}
