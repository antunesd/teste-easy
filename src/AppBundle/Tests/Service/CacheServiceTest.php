<?php

namespace AppBundle\Tests\Service;

use AppBundle\Service\CacheService;

class CacheServiceTest extends \PHPUnit_Framework_TestCase
{
    public function testClassShouldExists()
    {
        $this->assertInstanceOf(
            'AppBundle\Service\CacheService',
            new CacheService(null, null, null)
        );
    }

    public function testShouldRealizeFind()
    {
        $redisClientMock = $this->getMockBuilder('\Predis\Client')
            ->setMethods(array('lrange'))
            ->disableOriginalConstructor()
            ->getMock();

        $cacheServiceMock = $this->getMockBuilder('AppBundle\Service\CacheService')
            ->setMethods(array('getClient', 'getCollectionName'))
            ->disableOriginalConstructor()
            ->getMock();

        $cacheServiceMock
            ->method('getCollectionName')
            ->willReturn("customers");

        $cacheServiceMock
            ->method('getClient')
            ->willReturn($redisClientMock);

        $cacheServiceMock->find();
    }

    public function testShouldRealizeInsert()
    {
        $redisClientMock = $this->getMockBuilder('\Predis\Client')
            ->setMethods(array('lpush'))
            ->disableOriginalConstructor()
            ->getMock();

        $cacheServiceMock = $this->getMockBuilder('AppBundle\Service\CacheService')
            ->setMethods(array('getClient', 'getCollectionName'))
            ->disableOriginalConstructor()
            ->getMock();

        $cacheServiceMock
            ->method('getCollectionName')
            ->willReturn("customers");

        $cacheServiceMock
            ->method('getClient')
            ->willReturn($redisClientMock);

        $cacheServiceMock->insert(array());
    }

    public function testShouldRealizeDelete()
    {
        $redisClientMock = $this->getMockBuilder('\Predis\Client')
            ->setMethods(array('del'))
            ->disableOriginalConstructor()
            ->getMock();

        $cacheServiceMock = $this->getMockBuilder('AppBundle\Service\CacheService')
            ->setMethods(array('getClient', 'getCollectionName'))
            ->disableOriginalConstructor()
            ->getMock();

        $cacheServiceMock
            ->method('getCollectionName')
            ->willReturn("customers");

        $cacheServiceMock
            ->method('getClient')
            ->willReturn($redisClientMock);

        $cacheServiceMock->drop(array());
    }
}
