<?php

namespace AppBundle\Tests\Service;

use AppBundle\Service\DatabaseService;

class DatabaseServiceTest extends \PHPUnit_Framework_TestCase
{
    public function testClassShouldExists()
    {
        $this->assertInstanceOf(
            'AppBundle\Service\DatabaseService',
            new DatabaseService(null, null, null)
        );
    }
    
    public function testSetDatabase()
    {
        $databaseService = new DatabaseService(null, null, null);

        $mongoDbMock = $this->getMockBuilder('MongoDb')
            ->disableOriginalConstructor()
            ->getMock();

        $this->assertEquals(
            $databaseService,
            $databaseService->setDatabase($mongoDbMock)
        );

        $this->assertEquals(
            $mongoDbMock,
            $databaseService->getDatabase()
        );
    }
    
    public function testShouldRealizeFind()
    {
        $mongoDbMock = $this->getMockBuilder('MongoDb')
            ->disableOriginalConstructor()
            ->getMock();

        $mongoCollectionMock = $this->getMockBuilder('MongoCollection')
            ->disableOriginalConstructor()
            ->getMock();

        $mongoCursorMock = $this->getMockBuilder('MongoCursor')
            ->disableOriginalConstructor()
            ->getMock();
        
        $mongoCollectionMock
            ->method('find')
            ->willReturn($mongoCursorMock);

        $mongoDbMock->customers = $mongoCollectionMock;
 
        $databaseServiceMock = $this->getMockBuilder('AppBundle\Service\DatabaseService')
            ->setMethods(array('getDatabase', 'getCollectionName'))
            ->disableOriginalConstructor()
            ->getMock();

        $databaseServiceMock
            ->method('getDatabase')
            ->willReturn($mongoDbMock);

        $databaseServiceMock
            ->method('getCollectionName')
            ->willReturn("customers");

        $databaseServiceMock->setCollectionName("customers");

        $this->assertEquals(
            $mongoDbMock,
            $databaseServiceMock->getDatabase()
        );
        
        $this->assertTrue(
            is_array($databaseServiceMock->find())
        );
    }

    
    public function testShouldRealizeDelete()
    {
        $mongoDbMock = $this->getMockBuilder('MongoDb')
            ->disableOriginalConstructor()
            ->getMock();

        $mongoCollectionMock = $this->getMockBuilder('MongoCollection')
            ->disableOriginalConstructor()
            ->getMock();

        $mongoCursorMock = $this->getMockBuilder('MongoCursor')
            ->disableOriginalConstructor()
            ->getMock();
        
        $mongoCollectionMock
            ->expects($this->once())
            ->method('drop');

        $mongoDbMock->customers = $mongoCollectionMock;
 
        $databaseServiceMock = $this->getMockBuilder('AppBundle\Service\DatabaseService')
            ->setMethods(array('getDatabase', 'getCollectionName'))
            ->disableOriginalConstructor()
            ->getMock();

        $databaseServiceMock
            ->expects($this->once())
            ->method('getDatabase')
            ->willReturn($mongoDbMock);

        $databaseServiceMock
            ->expects($this->once())
            ->method('getCollectionName')
            ->willReturn("customers");

        $databaseServiceMock->setCollectionName("customers");

        $databaseServiceMock->drop();
    }

    public function testShouldRealizeInsert()
    {
        $mongoDbMock = $this->getMockBuilder('MongoDb')
            ->disableOriginalConstructor()
            ->getMock();

        $mongoCollectionMock = $this->getMockBuilder('MongoCollection')
            ->disableOriginalConstructor()
            ->getMock();

        $mongoCursorMock = $this->getMockBuilder('MongoCursor')
            ->disableOriginalConstructor()
            ->getMock();
        
        $mongoCollectionMock
            ->expects($this->once())
            ->method('insert');

        $mongoDbMock->customers = $mongoCollectionMock;
 
        $databaseServiceMock = $this->getMockBuilder('AppBundle\Service\DatabaseService')
            ->setMethods(array('getDatabase', 'getCollectionName'))
            ->disableOriginalConstructor()
            ->getMock();

        $databaseServiceMock
            ->expects($this->once())
            ->method('getDatabase')
            ->willReturn($mongoDbMock);

        $databaseServiceMock
            ->expects($this->once())
            ->method('getCollectionName')
            ->willReturn("customers");

        $databaseServiceMock->setCollectionName("customers");

        $databaseServiceMock->insert(array());
    }
}
