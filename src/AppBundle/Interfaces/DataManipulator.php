<?php

namespace AppBundle\Interfaces;

interface DataManipulator
{
    public function connect();
    public function setCollectionName($collectionName);
    public function insert($data);
    public function find();
    public function drop();
}
