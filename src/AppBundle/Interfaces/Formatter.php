<?php

namespace AppBundle\Interfaces;

interface Formatter
{
    public function format($data);
}
