EasyTaxi Cache Failover
========

Minimum Requirements
---------
* PHP 5.4
* [MongoDB driver](http://php.net/manual/en/mongo.installation.php#mongo.installation.nix)
* Mongo 2.6
* Redis 3

Installation
------
* $ git clone git@bitbucket.org:antunesd/teste-easy.git interview-cache-failover/
* $ cd interview-cache-failover/
* $ composer install
* $ php app/console server:run

* Start your **Redis** server 
* Start your **Mongo** server

* From METHOD "**GET**", you will receive data from any of the servers started (Cache or Database)
* From METHOD "**POST**" or "**DELETE**", you will receive a status code 500 if any of the servers is down

Post
------
* You can test your database operation by doing a **POST** into http://127.0.0.1:8000/customers/ 
  with raw body:
```
#!json

[{"name":"leandro", "age":26}, {"name":"marcio", "age":30}]
```


* Then check your MongoDB collection and Redis Client to see if customers were created 

Get
------
* You can test your database operation by doing a **GET** into http://127.0.0.1:8000/customers/ to receive the customers created from Redis (or Mongo if Redis is down)

Delete
------
* You can test your database operation by doing a **DELETE** into http://127.0.0.1:8000/customers/

* Then check your MongoDB collection and Redis Client to see if customers were deleted

Running tests
------

* Up the Mongo and Redis servers for Functional tests
* In project root, run bin/phpunit